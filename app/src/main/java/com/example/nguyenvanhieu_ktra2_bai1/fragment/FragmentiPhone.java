package com.example.nguyenvanhieu_ktra2_bai1.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nguyenvanhieu_ktra2_bai1.R;


public class FragmentiPhone extends Fragment {
    private TextView txtContent;
    private ImageView img;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragmenti_phone, container, false);
        img = v.findViewById(R.id.img);
        txtContent = v.findViewById(R.id.txtContent);

        img.setImageResource(R.drawable.iphone);
        txtContent.setText("Iphone 12 pro max");
        return v;
    }
}