package com.example.nguyenvanhieu_ktra2_bai1.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private int numPager = 3;
    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new FragmentSamsung();
            case 1: return new FragmentiPhone();
            case 2: return new FragmentOPPO();
            default: return new FragmentSamsung();
        }
    }

    @Override
    public int getCount() {
        return numPager;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "Samsung";
            case 1: return "iPhone";
            case 2: return "OPPO";
            default: return "Samsung";
        }
    }
}
