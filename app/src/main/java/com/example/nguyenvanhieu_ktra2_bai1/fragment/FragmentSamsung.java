package com.example.nguyenvanhieu_ktra2_bai1.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nguyenvanhieu_ktra2_bai1.R;


public class FragmentSamsung extends Fragment {
    private TextView txtContent;
    private ImageView img;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_samsung, container, false);
        img = v.findViewById(R.id.img);
        txtContent = v.findViewById(R.id.txtContent);

        img.setImageResource(R.drawable.samsung);
        txtContent.setText("Sam sung Note 20");
        return v;
    }
}